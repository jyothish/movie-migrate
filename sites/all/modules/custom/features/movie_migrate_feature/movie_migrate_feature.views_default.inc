<?php
/**
 * @file
 * movie_migrate_feature.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function movie_migrate_feature_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'movies';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Movies';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Movies';
  $handler->display->display_options['use_more'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'infinite_scroll';
  $handler->display->display_options['pager']['options']['items_per_page'] = '4';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'title' => 'title',
    'field_actors' => 'field_actors',
    'field_awards' => 'field_awards',
    'body' => 'body',
    'field_country' => 'field_country',
    'field_director' => 'field_director',
    'field_genre' => 'field_genre',
    'field_imdb_rating_' => 'field_imdb_rating_',
    'field_imdb_votes' => 'field_imdb_votes',
    'field_language' => 'field_language',
    'field_metascore' => 'field_metascore',
    'field_rated' => 'field_rated',
    'field_released' => 'field_released',
    'field_runtime' => 'field_runtime',
    'field_writer' => 'field_writer',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Actors */
  $handler->display->display_options['fields']['field_actors']['id'] = 'field_actors';
  $handler->display->display_options['fields']['field_actors']['table'] = 'field_data_field_actors';
  $handler->display->display_options['fields']['field_actors']['field'] = 'field_actors';
  $handler->display->display_options['fields']['field_actors']['label'] = '';
  $handler->display->display_options['fields']['field_actors']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_actors']['element_label_colon'] = FALSE;
  /* Field: Content: Awards */
  $handler->display->display_options['fields']['field_awards']['id'] = 'field_awards';
  $handler->display->display_options['fields']['field_awards']['table'] = 'field_data_field_awards';
  $handler->display->display_options['fields']['field_awards']['field'] = 'field_awards';
  $handler->display->display_options['fields']['field_awards']['label'] = '';
  $handler->display->display_options['fields']['field_awards']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_awards']['element_label_colon'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['exclude'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['text'] = '[body-summary] ';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Field: Content: Country */
  $handler->display->display_options['fields']['field_country']['id'] = 'field_country';
  $handler->display->display_options['fields']['field_country']['table'] = 'field_data_field_country';
  $handler->display->display_options['fields']['field_country']['field'] = 'field_country';
  $handler->display->display_options['fields']['field_country']['label'] = '';
  $handler->display->display_options['fields']['field_country']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_country']['element_label_colon'] = FALSE;
  /* Field: Content: Director */
  $handler->display->display_options['fields']['field_director']['id'] = 'field_director';
  $handler->display->display_options['fields']['field_director']['table'] = 'field_data_field_director';
  $handler->display->display_options['fields']['field_director']['field'] = 'field_director';
  $handler->display->display_options['fields']['field_director']['label'] = '';
  $handler->display->display_options['fields']['field_director']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_director']['element_label_colon'] = FALSE;
  /* Field: Content: Genre */
  $handler->display->display_options['fields']['field_genre']['id'] = 'field_genre';
  $handler->display->display_options['fields']['field_genre']['table'] = 'field_data_field_genre';
  $handler->display->display_options['fields']['field_genre']['field'] = 'field_genre';
  $handler->display->display_options['fields']['field_genre']['label'] = '';
  $handler->display->display_options['fields']['field_genre']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_genre']['element_label_colon'] = FALSE;
  /* Field: Content: IMDB Rating  */
  $handler->display->display_options['fields']['field_imdb_rating_']['id'] = 'field_imdb_rating_';
  $handler->display->display_options['fields']['field_imdb_rating_']['table'] = 'field_data_field_imdb_rating_';
  $handler->display->display_options['fields']['field_imdb_rating_']['field'] = 'field_imdb_rating_';
  $handler->display->display_options['fields']['field_imdb_rating_']['label'] = '';
  $handler->display->display_options['fields']['field_imdb_rating_']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_imdb_rating_']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_imdb_rating_']['settings'] = array(
    'thousand_separator' => ' ',
    'decimal_separator' => '.',
    'scale' => '2',
    'prefix_suffix' => 1,
  );
  /* Field: Content: IMDB Votes */
  $handler->display->display_options['fields']['field_imdb_votes']['id'] = 'field_imdb_votes';
  $handler->display->display_options['fields']['field_imdb_votes']['table'] = 'field_data_field_imdb_votes';
  $handler->display->display_options['fields']['field_imdb_votes']['field'] = 'field_imdb_votes';
  $handler->display->display_options['fields']['field_imdb_votes']['label'] = '';
  $handler->display->display_options['fields']['field_imdb_votes']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_imdb_votes']['element_label_colon'] = FALSE;
  /* Field: Content: Language */
  $handler->display->display_options['fields']['field_language']['id'] = 'field_language';
  $handler->display->display_options['fields']['field_language']['table'] = 'field_data_field_language';
  $handler->display->display_options['fields']['field_language']['field'] = 'field_language';
  $handler->display->display_options['fields']['field_language']['label'] = '';
  $handler->display->display_options['fields']['field_language']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_language']['element_label_colon'] = FALSE;
  /* Field: Content: Metascore */
  $handler->display->display_options['fields']['field_metascore']['id'] = 'field_metascore';
  $handler->display->display_options['fields']['field_metascore']['table'] = 'field_data_field_metascore';
  $handler->display->display_options['fields']['field_metascore']['field'] = 'field_metascore';
  $handler->display->display_options['fields']['field_metascore']['label'] = '';
  $handler->display->display_options['fields']['field_metascore']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_metascore']['element_label_colon'] = FALSE;
  /* Field: Content: Rated */
  $handler->display->display_options['fields']['field_rated']['id'] = 'field_rated';
  $handler->display->display_options['fields']['field_rated']['table'] = 'field_data_field_rated';
  $handler->display->display_options['fields']['field_rated']['field'] = 'field_rated';
  $handler->display->display_options['fields']['field_rated']['label'] = '';
  $handler->display->display_options['fields']['field_rated']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_rated']['element_label_colon'] = FALSE;
  /* Field: Content: Released */
  $handler->display->display_options['fields']['field_released']['id'] = 'field_released';
  $handler->display->display_options['fields']['field_released']['table'] = 'field_data_field_released';
  $handler->display->display_options['fields']['field_released']['field'] = 'field_released';
  $handler->display->display_options['fields']['field_released']['label'] = '';
  $handler->display->display_options['fields']['field_released']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_released']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_released']['settings'] = array(
    'format_type' => 'medium',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Content: Runtime */
  $handler->display->display_options['fields']['field_runtime']['id'] = 'field_runtime';
  $handler->display->display_options['fields']['field_runtime']['table'] = 'field_data_field_runtime';
  $handler->display->display_options['fields']['field_runtime']['field'] = 'field_runtime';
  $handler->display->display_options['fields']['field_runtime']['label'] = '';
  $handler->display->display_options['fields']['field_runtime']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_runtime']['element_label_colon'] = FALSE;
  /* Field: Content: Writer */
  $handler->display->display_options['fields']['field_writer']['id'] = 'field_writer';
  $handler->display->display_options['fields']['field_writer']['table'] = 'field_data_field_writer';
  $handler->display->display_options['fields']['field_writer']['field'] = 'field_writer';
  $handler->display->display_options['fields']['field_writer']['label'] = '';
  $handler->display->display_options['fields']['field_writer']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_writer']['element_label_colon'] = FALSE;
  /* Field: Content: Poster */
  $handler->display->display_options['fields']['field_poster']['id'] = 'field_poster';
  $handler->display->display_options['fields']['field_poster']['table'] = 'field_data_field_poster';
  $handler->display->display_options['fields']['field_poster']['field'] = 'field_poster';
  $handler->display->display_options['fields']['field_poster']['label'] = '';
  $handler->display->display_options['fields']['field_poster']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_poster']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_poster']['alter']['text'] = '[field_poster-value] ';
  $handler->display->display_options['fields']['field_poster']['element_label_colon'] = FALSE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="row" >  
<div class="col-md-4" > <img class="img-responsive" src="[field_poster] "  alt="[title]" > </div>

<div class="col-md-8" > 
<div class="row" > 
<div class=col-md-12" > <h4>Title </h4> [title]  </div>
<div class=col-md-12" ><h4>Actors </h4> [field_actors]  </div>
<div class=col-md-12" ><h4>Awards </h4> [field_awards]  </div>
<div class=col-md-12" ><h4>Plot</h4> [body]   </div>
<div class=col-md-12" ><h4>Country </h4> [field_country]  </div>
<div class=col-md-12" ><h4>Director </h4> [field_director] </div>
<div class=col-md-12" ><h4>Writer </h4> [field_writer] </div>
<div class=col-md-12" ><h4>Genre </h4> [field_genre]  </div>
<div class=col-md-12" ><h4>IMDB Rating </h4> [field_imdb_rating_]</div>
<div class=col-md-12" ><h4>Votes </h4> [field_imdb_votes] </div>
<div class=col-md-12" ><h4>Language</h4> [field_language]  </div>
<div class=col-md-12" ><h4>MetaScore </h4> [field_metascore] </div>
<div class=col-md-12" ><h4>Rating </h4> [field_rated] </div>
<div class=col-md-12" ><h4>Released on</h4> [field_released] </div>
<div class=col-md-12" ><h4>Run time </h4> [field_runtime] </div>


</div>

</div>

</div>
<hr>

';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'movies' => 'movies',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'movies';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Movies';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $export['movies'] = $view;

  return $export;
}
