<?php
/**
 * @file
 * movie_migrate_feature.features.inc
 */

/**
 * Implements hook_views_api().
 */
function movie_migrate_feature_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function movie_migrate_feature_node_info() {
  $items = array(
    'movies' => array(
      'name' => t('Movies'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Movie Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
